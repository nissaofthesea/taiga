# taiga

`taiga` is a music tagger that lets you edit tags with your text editor.

instead of providing a builtin editor, `taiga` dumps tags to a plaintext format.
after you've edited them, `taiga` merges the plaintext tags back into your
music.

## milestones

this is a work in progress! i'm still experimenting with this concept to
see if it's actually useful compared to traditional music taggers.

- [X] support common music tag formats
- support saving in different plaintext formats
  - [X] csv
  - [ ] toml
  - [ ] json
- [X] verbose logging
- [X] support comments
- [X] support lyrics
- [ ] manage cover art
- convenience features
  - [X] recursively select all music files in directory
  - [X] customize which properties to edit
