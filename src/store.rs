// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under GPL-3.0-or-later

use anyhow::{anyhow, Context};
use lofty::{
    AudioFile, ItemKey, MimeType, ParseOptions, ParsingMode, Picture, PictureType, Probe,
    Tag as LoftyTag, TaggedFile, TaggedFileExt,
};
use tracing::{field, span, Level};
use walkdir::WalkDir;

use std::ffi::OsString;
use std::fs::{self, OpenOptions};
use std::io::ErrorKind;
use std::path::{Path, PathBuf};

use crate::{Format, Tag};

const PARSE_MODE: ParsingMode = ParsingMode::BestAttempt;

/// Read a [`LoftyTag`] from the given path.
pub fn read_lofty_tag(path: &Path) -> anyhow::Result<LoftyTag> {
    let mut tagged_file: TaggedFile = Probe::open(path)?
        .options(
            ParseOptions::new()
                .read_properties(true)
                .parsing_mode(PARSE_MODE),
        )
        .read()?;
    tagged_file
        .remove(tagged_file.primary_tag_type())
        .ok_or(anyhow!("no primary tag found"))
}

/// Write a [`LoftyTag`] to the given path.
pub fn write_lofty_tag(tag: LoftyTag, path: &Path) -> anyhow::Result<()> {
    let mut tagged_file: TaggedFile = Probe::open(path)?
        .options(
            ParseOptions::new()
                .read_properties(true)
                .parsing_mode(PARSE_MODE),
        )
        .read()?;
    let dst = tagged_file
        .primary_tag_mut()
        .ok_or(anyhow!("no primary tag found"))?;
    *dst = tag;
    tagged_file.save_to_path(path)?;
    Ok(())
}

fn get_cover_front(tag: &LoftyTag) -> Option<&Picture> {
    let pic_count = tag.picture_count();
    for (idx, pic) in tag.pictures().iter().enumerate() {
        match (pic.pic_type(), pic_count, idx) {
            (PictureType::CoverFront, _, _) | (_, 1, 0) => return Some(pic),
            _ => (),
        }
    }
    None
}

fn get_cover_back(tag: &LoftyTag) -> Option<&Picture> {
    return tag
        .pictures()
        .iter()
        .find(|&pic| pic.pic_type() == PictureType::CoverBack);
}

#[derive(Debug)]
struct MetaFile {
    path: PathBuf, // relative to Store root
    format: Format,
}

impl MetaFile {
    pub fn new(format: Format) -> Self {
        let mut path = PathBuf::from("meta");
        path.set_extension(format.extension());
        Self { path, format }
    }
}

#[derive(Debug)]
pub struct Target {
    store_path: PathBuf, // relative to Store root
    // HACK
    pub tag: Tag,
    pub cover_front: Option<Picture>,
    pub cover_back: Option<Picture>,
    pub lyrics: Option<String>,
}

impl Target {
    pub fn compute_store_path(src_path: &Path) -> PathBuf {
        // TODO: detect collisions between target store paths
        let mut s = OsString::new();
        for (i, component) in src_path.components().enumerate() {
            if i != 0 {
                s.push("_");
            }
            if let Some(component) = PathBuf::from(component.as_os_str()).file_prefix() {
                s.push(component);
            }
        }
        s.into()
    }

    pub fn read_from_src(src_path: PathBuf) -> anyhow::Result<Self> {
        /* compute store path */
        let store_path: PathBuf = Self::compute_store_path(&src_path);

        /* read tag from src_path */
        let lofty_tag = read_lofty_tag(&src_path)?;

        let mut cover_front = None;
        let mut cover_back = None;
        let mut cover_front_path = None;
        let mut cover_back_path = None;

        for (try_pic, data_dst, path_dst, name) in [
            (
                get_cover_front(&lofty_tag),
                &mut cover_front,
                &mut cover_front_path,
                "cover_front",
            ),
            (
                get_cover_back(&lofty_tag),
                &mut cover_back,
                &mut cover_back_path,
                "cover_back",
            ),
        ] {
            if let Some(pic) = try_pic {
                let extension = match pic.mime_type() {
                    MimeType::Png => "png",
                    MimeType::Jpeg => "jpg",
                    MimeType::Tiff => "tif",
                    MimeType::Bmp => "bmp",
                    MimeType::Gif => "gif",
                    MimeType::Unknown(ext) => ext,
                    MimeType::None => "bin",
                    _ => "unknown",
                };

                let mut path = store_path.clone();
                path.push("cover_front");
                path.set_extension(extension);

                *data_dst = Some(pic.clone());
                *path_dst = Some(path);
            } else {
                // TODO: this is minimally helpful
                tracing::trace!("{name} not found");
            }
        }

        let mut lyrics = None;
        let mut lyrics_path = None;
        if let Some(data) = lofty_tag.get_string(&ItemKey::Lyrics) {
            lyrics_path = Some({
                let mut path = store_path.clone();
                path.push("lyrics");
                path.set_extension("txt");
                path
            });
            lyrics = Some(data.to_string());
        }

        let tag = Tag::from_lofty_tag(
            src_path,
            cover_front_path,
            cover_back_path,
            lyrics_path,
            &lofty_tag,
        );

        Ok(Self {
            store_path,
            tag,
            cover_front,
            cover_back,
            lyrics,
        })
    }

    pub fn store_path(&self, store: &Store) -> PathBuf {
        store.path_fmt(&self.store_path)
    }
}

/// Represents the structure and location of a store.
#[derive(Debug)]
pub struct Store {
    root: PathBuf,
    meta: MetaFile,
    targets: Vec<Target>,
}

impl Store {
    /// Returns a new `Store`, representing a store in the filesystem.
    #[must_use]
    pub fn new(root: PathBuf, meta_format: Format) -> Self {
        Self {
            root,
            meta: MetaFile::new(meta_format),
            targets: Vec::new(),
        }
    }

    /// Write the `Store` to the filesystem. In other words, the in-memory
    /// `Store` representation will be implemented in the filesystem. This does
    /// not mutate the targets.
    ///
    /// # Errors
    ///
    /// - Writing to the filesystem may fail.
    /// - If `overwrite` is `true`, overwriting an existing `Store` is an error.
    pub fn write(&self) -> anyhow::Result<()> {
        // create backup
        let bak = self.bak_path();
        assert_ne!(self.root, bak);
        tracing::trace!("removing previous backup store");
        if let Err(err) = fs::remove_dir_all(&bak) {
            match err.kind() {
                ErrorKind::NotFound => (),
                _ => return Err(err).context("failed to remove previous backup store"),
            }
        }
        tracing::trace!("creating backing store");
        fs::rename(&self.root, &bak).context("failed to rename store to backup")?;

        // create root
        fs::create_dir_all(&self.root).context("failed to create store root")?;

        // create meta file
        let mut meta = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(self.meta_path())
            .context("failed to open meta file")?;

        self.meta
            .format
            .write_plaintext_tag(&mut meta, self.targets.iter().map(|target| &target.tag))
            .context("failed to write meta file")?;

        // create target dirs
        for target in &self.targets {
            fs::create_dir_all(target.store_path(self))
                .context("failed to create target directories")?;
        }

        // write offloaded tags to store
        for target in &self.targets {
            let tag: &Tag = &target.tag;
            if let Some(ref rel_dst) = tag.cover_front {
                let front = target.cover_front.as_ref().unwrap();
                let dst = self.path_fmt(rel_dst);
                fs::write(&dst, front.data()).context("failed to write cover_front to store")?;
            }
            if let Some(ref rel_dst) = tag.cover_back {
                let back = target.cover_back.as_ref().unwrap();
                let dst = self.path_fmt(rel_dst);
                fs::write(&dst, back.data()).context("failed to write cover_back to store")?;
            }
            if let Some(ref rel_dst) = tag.lyrics_path {
                let lyrics = target.lyrics.as_ref().unwrap();
                let dst = self.path_fmt(rel_dst);
                fs::write(&dst, lyrics).context("failed to write lyrics to store")?;
            }
        }

        Ok(())
    }

    // TODO: meta-file format shouldn't matter for reading, only writing.
    /// Read a `Store` from the `self`'s root path, given that the meta-file
    /// format matches. This reads the store into Rust memory, and does not
    /// mutate the filesystem.
    ///
    /// # Errors
    ///
    /// - Reading from the filesystem may fail.
    /// - The store may be invalid or contain invalid data.
    pub fn read(&mut self) -> anyhow::Result<()> {
        let span = tracing::span!(
            Level::INFO,
            "store_read",
            meta_path = format_args!("{}", self.meta_path().display())
        );
        let _enter = span.enter();
        tracing::trace!("reading meta file");
        let meta = OpenOptions::new()
            .read(true)
            .open(self.meta_path())
            .context("failed to open meta file")?;
        let tags = self
            .meta
            .format
            .read_plaintext_tags(meta)
            .context("failed to read meta file")?;
        self.targets = Vec::with_capacity(tags.len());
        tracing::trace!("reading tags from store");
        for tag in tags {
            let span = tracing::span!(
                Level::INFO,
                "read_targets",
                cover_front_path = field::Empty,
                cover_back_path = field::Empty,
                lyrics_path = field::Empty,
            );
            let _enter = span.enter();

            let mut cover_front = None;
            let mut cover_back = None;
            let mut lyrics = None;

            if let Some(ref cf_path) = tag.cover_front {
                span.record("cover_front_path", format_args!("{}", cf_path.display()));
                tracing::trace!("reading offloaded cover_front");
                let src = self.path_fmt(cf_path);
                let mut file = OpenOptions::new()
                    .read(true)
                    .open(src)
                    .context("failed to open offloaded cover_front")?;
                cover_front = Some(
                    Picture::from_reader(&mut file)
                        .context("failed to read Picture from offloaded cover_front")?,
                );
            }
            if let Some(ref cb_path) = tag.cover_back {
                span.record("cover_back_path", format_args!("{}", cb_path.display()));
                tracing::trace!("reading offloaded cover_back");
                let src = self.path_fmt(cb_path);
                let mut file = OpenOptions::new().read(true).open(src)?;
                cover_back = Some(
                    Picture::from_reader(&mut file)
                        .context("failed to read Picture from offloaded cover_back")?,
                );
            }
            if let Some(ref lyrics_path) = tag.lyrics_path {
                span.record("lyrics_path", format_args!("{}", lyrics_path.display()));
                tracing::trace!("reading offloaded lyrics");
                let src = self.path_fmt(lyrics_path);
                lyrics = Some(fs::read_to_string(src).context("failed to read offloaded lyrics")?);
            }

            let target = Target {
                store_path: Target::compute_store_path(&tag.path),
                tag,
                cover_front,
                cover_back,
                lyrics,
            };
            self.targets.push(target);
        }
        Ok(())
    }

    /// Add `path` to the store as a target.
    ///
    /// # Errors
    ///
    /// - Reading the target from the filesystem may fail.
    /// - Parsing the target's metadata may fail.
    pub fn add_target(&mut self, path: &Path) -> anyhow::Result<()> {
        /* expand directories into files within */
        for entry in WalkDir::new(path).sort_by(|a, b| a.file_name().cmp(b.file_name())) {
            let entry = entry?;

            let span = span!(
                Level::INFO,
                "entry",
                path = entry.path().display().to_string()
            );
            let _guard = span.enter();

            // skip directories and symlinks
            if entry.file_type().is_file() {
                /* construct target */
                match Target::read_from_src(entry.path().into()) {
                    Ok(target) => {
                        tracing::info!("adding target");
                        self.targets.push(target);
                    }

                    Err(error) => {
                        tracing::error!("failed to construct Target: {}", error);
                    }
                }
            }
        }

        Ok(())
    }

    /// Return a reference to the targets in the store.
    #[must_use]
    pub fn targets(&self) -> &[Target] {
        &self.targets
    }

    /// Return the store's targets, consuming the store.
    #[must_use]
    pub fn take_targets(self) -> Vec<Target> {
        self.targets
    }

    fn meta_path(&self) -> PathBuf {
        self.path_fmt(&self.meta.path)
    }

    fn bak_path(&self) -> PathBuf {
        let mut bak = self.root.clone();
        let mut name = bak.file_name().unwrap().to_owned();
        name.push(".bak");
        bak.set_file_name(name);
        bak
    }

    fn path_fmt(&self, path: &Path) -> PathBuf {
        let mut p = self.root.clone();
        p.push(path);
        p
    }
}
