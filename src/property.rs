// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under GPL-3.0-or-later

use clap::ValueEnum;

/// Enumeration over supported music properties.
#[allow(missing_docs)]
#[derive(ValueEnum, Clone, Copy, Debug)]
pub enum Property {
    Album,
    AlbumArtist,
    Artist,
    Comment,
    Date,
    Disc,
    Genre,
    Lyrics,
    Title,
    TotalDiscs,
    TotalTracks,
    Track,
}
