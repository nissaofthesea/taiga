// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under GPL-3.0-or-later

use lofty::{Accessor, ItemKey, ItemValue, Tag as LoftyTag, TagItem};
use serde_derive::{Deserialize, Serialize};

use std::path::PathBuf;

/// Common denominator audio tag.
#[derive(Clone, Deserialize, Serialize, Debug)]
pub struct Tag {
    /// Path of the original audio file.
    pub path: PathBuf,
    /// Path to the audio file's primary cover.
    pub cover_front: Option<PathBuf>,
    /// Path to the audio file's secondary cover.
    pub cover_back: Option<PathBuf>,
    /// Path to the audio file's lyrics.
    pub lyrics_path: Option<PathBuf>,
    #[serde(skip_serializing_if = "state::skip_album")]
    album: Option<String>,
    #[serde(skip_serializing_if = "state::skip_album_artist")]
    album_artist: Option<String>,
    #[serde(skip_serializing_if = "state::skip_artist")]
    artist: Option<String>,
    #[serde(skip_serializing_if = "state::skip_comment")]
    comment: Option<String>,
    #[serde(skip_serializing_if = "state::skip_date")]
    date: Option<String>,
    #[serde(skip_serializing_if = "state::skip_disc")]
    disc: Option<u32>,
    #[serde(skip_serializing_if = "state::skip_genre")]
    genre: Option<String>,
    #[serde(skip_serializing_if = "state::skip_title")]
    title: Option<String>,
    #[serde(skip_serializing_if = "state::skip_total_discs")]
    total_discs: Option<u32>,
    #[serde(skip_serializing_if = "state::skip_total_tracks")]
    total_tracks: Option<u32>,
    #[serde(skip_serializing_if = "state::skip_track")]
    track: Option<u32>,
}

impl Tag {
    /// Returns a new blank [`Tag`].
    #[must_use]
    pub const fn new(
        path: PathBuf,
        cover_front: Option<PathBuf>,
        cover_back: Option<PathBuf>,
        lyrics_path: Option<PathBuf>,
    ) -> Self {
        Self {
            path,
            cover_front,
            cover_back,
            lyrics_path,
            album: None,
            album_artist: None,
            artist: None,
            comment: None,
            date: None,
            disc: None,
            genre: None,
            title: None,
            total_discs: None,
            total_tracks: None,
            track: None,
        }
    }

    /// Populates a [`Tag`] from the `path` to the audio file and a [lofty
    /// tag](LoftyTag).
    #[must_use]
    pub fn from_lofty_tag(
        path: PathBuf,
        cover_front: Option<PathBuf>,
        cover_back: Option<PathBuf>,
        lyrics_path: Option<PathBuf>,
        lofty: &LoftyTag,
    ) -> Self {
        let mut tag = Tag::new(path, cover_front, cover_back, lyrics_path);

        tag.album = lofty.album().map(String::from);
        tag.album_artist = lofty.get_string(&ItemKey::AlbumArtist).map(str::to_string);
        tag.artist = lofty.artist().map(String::from);
        tag.comment = lofty.comment().map(String::from);
        tag.date = lofty
            .get_string(&ItemKey::RecordingDate)
            .map(str::to_string);
        tag.disc = lofty.disk();
        tag.genre = lofty.genre().map(String::from);
        tag.title = lofty.title().map(String::from);
        tag.total_discs = lofty.disk_total();
        tag.total_tracks = lofty.track_total();
        tag.track = lofty.track();

        tag
    }

    /// Applies the [`Tag`]'s properties to the given [lofty tag](LoftyTag).
    ///
    /// Returns a tuple containing the new [lofty tag](LoftyTag) and the
    /// [`Tag`]'s path.
    #[must_use]
    pub fn to_lofty_tag(self, mut tag: LoftyTag) -> (LoftyTag, PathBuf) {
        if let Some(album) = self.album {
            tag.set_album(album);
        }

        if let Some(album_artist) = self.album_artist {
            tag.insert(TagItem::new(
                ItemKey::AlbumArtist,
                ItemValue::Text(album_artist),
            ));
        }

        if let Some(artist) = self.artist {
            tag.set_artist(artist);
        }

        if let Some(comment) = self.comment {
            tag.set_comment(comment);
        }

        if let Some(date) = self.date {
            tag.insert(TagItem::new(ItemKey::RecordingDate, ItemValue::Text(date)));
        }

        if let Some(disc) = self.disc {
            tag.set_disk(disc);
        }

        if let Some(genre) = self.genre {
            tag.set_genre(genre);
        }

        if let Some(title) = self.title {
            tag.set_title(title);
        }

        if let Some(total_discs) = self.total_discs {
            tag.set_disk_total(total_discs);
        }

        if let Some(total_tracks) = self.total_tracks {
            tag.set_track_total(total_tracks);
        }

        if let Some(track) = self.track {
            tag.set_track(track);
        }

        (tag, self.path)
    }
}

/// State of selected properties.
#[allow(clippy::trivially_copy_pass_by_ref)]
pub mod state {
    // TODO: im sure atomic orderings could be loosened in some places. im using
    // seqcst bc i dont know better.

    use core::sync::atomic::{AtomicBool, Ordering};

    use crate::Property;

    /// Global include list of properties.
    pub static STATE: Include = Include::new();

    /// Include list of properties.
    ///
    /// If the value is true, then the property should be included.
    pub struct Include {
        album: AtomicBool,
        album_artist: AtomicBool,
        artist: AtomicBool,
        comment: AtomicBool,
        date: AtomicBool,
        disc: AtomicBool,
        genre: AtomicBool,
        lyrics: AtomicBool,
        title: AtomicBool,
        total_discs: AtomicBool,
        total_tracks: AtomicBool,
        track: AtomicBool,
    }

    impl Include {
        /// Returns a new rule allowing all properties.
        #[must_use]
        pub const fn new() -> Self {
            Self {
                album: AtomicBool::new(true),
                album_artist: AtomicBool::new(true),
                artist: AtomicBool::new(true),
                comment: AtomicBool::new(true),
                date: AtomicBool::new(true),
                disc: AtomicBool::new(true),
                genre: AtomicBool::new(true),
                lyrics: AtomicBool::new(true),
                title: AtomicBool::new(true),
                total_discs: AtomicBool::new(true),
                total_tracks: AtomicBool::new(true),
                track: AtomicBool::new(true),
            }
        }

        /// Set all properties to `value`.
        pub fn set_all(&self, value: bool) {
            self.album.store(value, Ordering::SeqCst);
            self.album_artist.store(value, Ordering::SeqCst);
            self.artist.store(value, Ordering::SeqCst);
            self.comment.store(value, Ordering::SeqCst);
            self.date.store(value, Ordering::SeqCst);
            self.disc.store(value, Ordering::SeqCst);
            self.genre.store(value, Ordering::SeqCst);
            self.lyrics.store(value, Ordering::SeqCst);
            self.title.store(value, Ordering::SeqCst);
            self.total_discs.store(value, Ordering::SeqCst);
            self.total_tracks.store(value, Ordering::SeqCst);
            self.track.store(value, Ordering::SeqCst);
        }

        /// Sets the given property to `value`.
        pub fn set_property(&self, prop: Property, value: bool) {
            match prop {
                Property::Album => self.album.store(value, Ordering::SeqCst),
                Property::AlbumArtist => self.album_artist.store(value, Ordering::SeqCst),
                Property::Artist => self.artist.store(value, Ordering::SeqCst),
                Property::Comment => self.comment.store(value, Ordering::SeqCst),
                Property::Date => self.date.store(value, Ordering::SeqCst),
                Property::Disc => self.disc.store(value, Ordering::SeqCst),
                Property::Genre => self.genre.store(value, Ordering::SeqCst),
                Property::Lyrics => self.lyrics.store(value, Ordering::SeqCst),
                Property::Title => self.title.store(value, Ordering::SeqCst),
                Property::TotalDiscs => self.total_discs.store(value, Ordering::SeqCst),
                Property::TotalTracks => self.total_tracks.store(value, Ordering::SeqCst),
                Property::Track => self.track.store(value, Ordering::SeqCst),
            }
        }

        /// Gets the value of the given property.
        pub fn get_property(&self, prop: Property) -> bool {
            match prop {
                Property::Album => self.album.load(Ordering::SeqCst),
                Property::AlbumArtist => self.album_artist.load(Ordering::SeqCst),
                Property::Artist => self.artist.load(Ordering::SeqCst),
                Property::Comment => self.comment.load(Ordering::SeqCst),
                Property::Date => self.date.load(Ordering::SeqCst),
                Property::Disc => self.disc.load(Ordering::SeqCst),
                Property::Genre => self.genre.load(Ordering::SeqCst),
                Property::Lyrics => self.lyrics.load(Ordering::SeqCst),
                Property::Title => self.title.load(Ordering::SeqCst),
                Property::TotalDiscs => self.total_discs.load(Ordering::SeqCst),
                Property::TotalTracks => self.total_tracks.load(Ordering::SeqCst),
                Property::Track => self.track.load(Ordering::SeqCst),
            }
        }
    }

    pub(super) fn skip_album(_: &Option<String>) -> bool {
        !STATE.album.load(Ordering::SeqCst)
    }

    pub(super) fn skip_album_artist(_: &Option<String>) -> bool {
        !STATE.album_artist.load(Ordering::SeqCst)
    }

    pub(super) fn skip_artist(_: &Option<String>) -> bool {
        !STATE.artist.load(Ordering::SeqCst)
    }

    pub(super) fn skip_comment(_: &Option<String>) -> bool {
        !STATE.comment.load(Ordering::SeqCst)
    }

    pub(super) fn skip_date(_: &Option<String>) -> bool {
        !STATE.date.load(Ordering::SeqCst)
    }

    pub(super) fn skip_disc(_: &Option<u32>) -> bool {
        !STATE.disc.load(Ordering::SeqCst)
    }

    pub(super) fn skip_genre(_: &Option<String>) -> bool {
        !STATE.genre.load(Ordering::SeqCst)
    }

    pub(super) fn skip_title(_: &Option<String>) -> bool {
        !STATE.title.load(Ordering::SeqCst)
    }

    pub(super) fn skip_total_discs(_: &Option<u32>) -> bool {
        !STATE.total_discs.load(Ordering::SeqCst)
    }

    pub(super) fn skip_total_tracks(_: &Option<u32>) -> bool {
        !STATE.total_tracks.load(Ordering::SeqCst)
    }

    pub(super) fn skip_track(_: &Option<u32>) -> bool {
        !STATE.track.load(Ordering::SeqCst)
    }
}
