// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under GPL-3.0-or-later

#![forbid(unsafe_code)]
#![warn(missing_docs, clippy::pedantic)]
#![feature(path_file_prefix, io_error_more)]

//! Read tag formats into common denominator [`Tag`] and write them to
//! plaintext.

mod format;
mod property;
mod store;
mod tag;

pub use format::Format;
pub use property::Property;
pub use store::{read_lofty_tag, write_lofty_tag, Store, Target};
pub use tag::state;
pub use tag::Tag;
