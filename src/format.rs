// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under GPL-3.0-or-later

use crate::tag::Tag;

use anyhow::Context;
use clap::ValueEnum;

use std::io::{Read, Write};

/// Different formats for plaintext tags.
#[derive(ValueEnum, Clone, Copy, Debug, Default)]
pub enum Format {
    /// CSV (comma separated values)
    #[default]
    Csv,
}

impl Format {
    /// Returns the format extension.
    #[must_use]
    pub const fn extension(&self) -> &'static str {
        match self {
            Self::Csv => "csv",
        }
    }

    /// Serializes tags in the given [`Format`], writing to `w`.
    pub fn write_plaintext_tag<'a>(
        &self,
        w: impl Write,
        tags: impl IntoIterator<Item = &'a Tag>,
    ) -> anyhow::Result<()> {
        match self {
            Self::Csv => {
                let mut w = csv::Writer::from_writer(w);
                for tag in tags {
                    w.serialize(tag)
                        .context("failed to write serialized tag to writer")?;
                }
                w.flush().context("failed to flush writer")?;
            }
        }

        Ok(())
    }

    /// Reads and deserializes tags in the given [`Format`] from `r`.
    pub fn read_plaintext_tags(&self, r: impl Read) -> anyhow::Result<Vec<Tag>> {
        let mut tags = Vec::new();

        match self {
            Self::Csv => {
                let mut r = csv::Reader::from_reader(r);
                for result in r.deserialize() {
                    tags.push(result?);
                }
            }
        }

        Ok(tags)
    }
}
