// copyright (C) 2022-2023 Nissa <and-nissa@protonmail.com>
// licensed under GPL-3.0-or-later

/* TODO: support relative paths */
/* TODO: automatically skip images when reading tags */
/* TODO: only rewrite tag if necessary */
/* TODO: who am i to decide what properties are big enough to offload to separate file? try to generalize code for that so user can specify property list of offloads */
/* TODO: option to clean up target dirs not present in targets */

#![forbid(unsafe_code)]
#![warn(clippy::pedantic)]

use anyhow::Context;
use clap::{Parser, Subcommand};
use lofty::{ItemKey, Tag as LoftyTag};
use tracing::{span, Level};

use std::io::stderr;
use std::path::PathBuf;
use std::process::ExitCode;

use taiga::{state::STATE, Format, Property, Store, Tag};

#[derive(Parser, Debug)]
#[clap(author, version, about, subcommand_required = true)]
struct Args {
    /// Path of metadata directory (known as the store)
    #[clap(short, long, value_name = "PATH", default_value = ".taiga")]
    store: PathBuf,

    /// Format to use for meta-file
    #[clap(short, long, value_enum, default_value_t = Format::default())]
    format: Format,

    /// Exclude specific properties from consideration
    #[clap(
        short = 'x',
        long,
        value_enum,
        conflicts_with = "include",
        ignore_case = true,
        num_args = 1..,
        use_value_delimiter = true,
    )]
    exclude: Vec<Property>,

    /// Only include specific properties for consideration
    #[clap(
        short,
        long,
        value_enum,
        conflicts_with = "exclude",
        ignore_case = true,
        num_args = 1..,
        use_value_delimiter = true,
    )]
    include: Vec<Property>,

    #[clap(subcommand)]
    mode: Mode,
}

#[derive(Subcommand, Debug)]
enum Mode {
    /// Read tags from targets and write plaintext to store
    Get {
        /// Path of target music file(s)
        #[clap(value_name = "PATH")]
        targets: Vec<PathBuf>,
    },

    /// Read plaintext tags from store and write to targets
    Set {
        /// Exclusively use tags from store, removing all other tags present in targets
        #[clap(short, long)]
        replace_all: bool,
    },
}

fn main() -> ExitCode {
    tracing_subscriber::fmt()
        .with_writer(stderr)
        .with_max_level(Level::TRACE)
        .with_timer(tracing_subscriber::fmt::time::time())
        .init();
    if let Err(error) = try_main(Args::parse()) {
        tracing::error!("{}", error);
        error
            .chain()
            .skip(1)
            .for_each(|cause| tracing::info!("because of: {}", cause));
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}

fn try_main(args: Args) -> anyhow::Result<()> {
    // customize properties based on include or exclude list
    if !args.exclude.is_empty() {
        STATE.set_all(true);

        for property in args.exclude {
            STATE.set_property(property, false);
        }
    } else if !args.include.is_empty() {
        STATE.set_all(false);

        for property in args.include {
            STATE.set_property(property, true);
        }
    }

    // create store
    let mut store = Store::new(args.store, args.format);

    /* dispatch on subcommand */
    match args.mode {
        Mode::Get { targets } => {
            {
                let span = span!(Level::INFO, "add_targets");
                let _guard = span.enter();
                for target in targets {
                    if let Err(error) = store.add_target(&target) {
                        tracing::error!("failed to add target, skipping: {}", error);
                    }
                }
            }

            store
                .write()
                .context("failed to write store to filesystem")?;
        }

        Mode::Set { replace_all } => {
            store.read().context("failed to read store")?;
            for mut target in store.take_targets() {
                let tag: Tag = target.tag;

                let span = tracing::span!(
                    Level::INFO,
                    "update_targets",
                    target_path = format!("{}", tag.path.display())
                );
                let _enter = span.enter();

                tracing::trace!("reading tag");
                let cur_ltag = taiga::read_lofty_tag(&tag.path).context("failed to read tag")?;
                let lofty_tag: LoftyTag = if replace_all {
                    LoftyTag::new(cur_ltag.tag_type())
                } else {
                    cur_ltag
                };
                tracing::trace!("updating tag");
                let (mut lofty_tag, target_path) = tag.to_lofty_tag(lofty_tag);

                let mut len: usize = lofty_tag.picture_count().try_into().unwrap();
                while 0 < len {
                    lofty_tag.remove_picture(len - 1);
                    len -= 1;
                }
                assert!(lofty_tag.picture_count() == 0);

                if let Some(cover_front) = target.cover_front.take() {
                    lofty_tag.push_picture(cover_front);
                }
                if let Some(cover_back) = target.cover_back.take() {
                    lofty_tag.push_picture(cover_back);
                }
                if let Some(lyrics) = target.lyrics.take() {
                    lofty_tag.insert_text(ItemKey::Lyrics, lyrics);
                }
                tracing::info!("writing updated tag");
                taiga::write_lofty_tag(lofty_tag, &target_path)
                    .context("failed to write updated tag to target")?;
            }
        }
    }

    Ok(())
}
