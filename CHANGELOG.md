# changelog

## [unreleased]
* added support for managing front and back covers
* feat: offload lyrics to separate file
* feat: added help message for `--exclude` and `--include`
* feat: store freshly created for each Set (and backup created when removing old store)
* removed `--overwrite` flag (backups largely remove risk)

## [0.5.0] - 2022-07-25
* feat: added `--exclude` and `--include` for selecting specific properties

## [0.4.1] - 2022-07-24
* fix: sort list of targets

## [0.4.0] - 2022-07-24
* feat: select meta file extension if none is present
* fix: no need to accept cli targets when setting

## [0.3.1] - 2022-07-24
* fix: fixed misaligned tag paths

## [0.3.0] - 2022-07-24
* feat: resolve dirs into files within
* feat: treat tag reading errors as warnings

## [0.2.0] - 2022-07-21
* support more metadata formats (see `lofty`'s [supported formats](https://docs.rs/lofty/latest/lofty/#supported-formats))
* support lyrics
* fixed `--overwrite` not fully overwriting previous file contents

## [0.1.0] - 2022-07-13
* feat: get metadata from target files and write to meta file
* feat: set metadata from meta file to target files
* feat: allow customizing meta file overwrite behavior
* feat: allow customizing tag replacement behavior
* feat: error traceback
* feat: verbose logging
